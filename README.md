# COURSE BOT API

Para utilizar este projeto é necessário instalar o [Redis](https://redis.io/)
e o banco de dados [PostgreSQL](https://www.postgresql.org/)

## Configuração

Será necessário definir algumas variáveis de sistema para que a aplicação funcione
corretamente. Essas variáveis são as credenciais da conta do Twitter que se
deseja utilizar:

``` ruby
  CONSUMER_KEY = 'XXXXXXXX'
  CONSUMER_SECRET = 'XXXXXXX'
  ACCESS_TOKEN = 'XXXXXXX'
  ACCESS_TOKEN_SECRET = 'XXXXXXX'
```

## Cadastrando um curso

Para cadastrar um curso e realizar a postagem de um tweet basta enviar uma
requisição para o endpoint `/api/v1/courses`, após este curso ter sido cadastrado,
caso o campo `postage_time` não tenha sido passado, o tweet será realizado
imediatamente, caso tenha sido passado, o tweet será agendado para a data e
hora definido.

``` json
  "course": {
    "title": "Title",
    "description": "description",
    "course_url": "http://course.com",
    "time_available": "2018-07-31 08:00:00",
    "is_paid": true,
    "price": 100.00,
    "workload": 40.5,
    "custom_message": "Message",
    "postage_time": "2018-07-31 08:00:00"
  }
```

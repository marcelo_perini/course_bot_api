# frozen_string_literal: true

# This service have the responsibility to create a course and
# queue a job to tweet the course description
class CreateCourseService
  def initialize(course)
    @course = course
  end

  def execute
    if @course.save
      queue_job
      true
    else
      false
    end
  end

  protected

  def queue_job
    if tweet_now?
      TweetJob.perform_later(@course)
    else
      TweetJob.set(wait_until: @course.postage_time).perform_later(@course)
    end
  end

  def tweet_now?
    @course.postage_time.present?
  end
end

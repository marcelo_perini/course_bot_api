# frozen_string_literal: true

# This class have the responsibility to tweet a new course description
class TweetCourseService
  def initialize(course, twitter = TWITTER_CLIENT)
    @course = course
    @twitter = twitter
  end

  def execute
    @twitter.update(course_message)
  end

  protected

  def course_message
    if @course.custom_message.present?
      @course.custom_message
    else
      "Course #{@course.title} - #{@course.description}
        (#{@course.course_url}). #{value_message}"
    end
  end

  def value_message
    return unless @course.is_paid
    "Price #{@course.price}."
  end
end

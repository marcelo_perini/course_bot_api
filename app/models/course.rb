# frozen_string_literal: true

class Course < ApplicationRecord
  validates :title, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 255 }
  validates :course_url, presence: true, length: { maximum: 50 }
  validates :time_available, presence: true
  validates :is_paid, presence: true
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :workload, presence: true, numericality: { greater_than: 0 }
  validates :custom_message, length: { maximum: 281 }
end

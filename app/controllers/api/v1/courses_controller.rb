class Api::V1::CoursesController < ApplicationController
  # POST /api/v1/course
  def create
    @course = Course.new course_params

    if CreateCourseService.new(@course).execute
      render json: { success: true }, status: :created
    else
      render json: { success: false}, status: :bad_request
    end
  end

  private

  def course_params
    params.require(:course).permit(:title, :description, :course_url,
                                   :time_available, :is_paid, :price, :workload,
                                   :custom_message, :postage_time)
  end
end

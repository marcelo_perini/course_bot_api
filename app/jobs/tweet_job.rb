class TweetJob < ApplicationJob
  queue_as :default

  def perform(course)
    TweetCourseService.new(course, TWITTER_CLIENT).execute
  end
end

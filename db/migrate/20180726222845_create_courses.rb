class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :title
      t.string :description
      t.string :course_url
      t.datetime :time_available
      t.boolean :is_paid
      t.decimal :price, precision: 30, scale: 2
      t.decimal :workload, precision: 30, scale: 2
      t.string :custom_message
      t.datetime :postage_time

      t.timestamps
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TweetCourseService, type: :service do
  describe '#execute' do
    let(:course) { create(:course) }

    it 'tweet a new message' do
      twitter = double('twitter')
      tweet_course_service = TweetCourseService.new(course, twitter)

      expect(twitter).to receive(:update).with(course.custom_message)

      tweet_course_service.execute
    end
  end
end

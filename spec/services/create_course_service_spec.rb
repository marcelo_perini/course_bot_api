# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateCourseService, type: :service do
  describe '#execute' do
    context 'with valid attributes' do
      let(:twitter) { double('twitter') }
      let(:course) { build(:course) }

      it 'returns true' do
        allow(twitter).to receive(:update).and_return(course.custom_message)

        create_course_service = CreateCourseService.new(course)
        expect(create_course_service.execute).to be_truthy
      end

      it 'persist course' do
        allow(twitter).to receive(:update).and_return(course.custom_message)

        CreateCourseService.new(course).execute

        expect(course).to be_persisted
      end
    end

    context 'with invalid attributes' do
      let(:course) { build(:course, title: nil) }
      let(:create_course_service) { CreateCourseService.new(course) }

      it 'returns false' do
        expect(create_course_service.execute).to be_falsy
      end

      it 'not persit course' do
        create_course_service.execute

        expect(course).not_to be_persisted
      end

      it 'has errors messages with course' do
        create_course_service.execute

        expect(course.errors.empty?).to be_falsy
      end
    end
  end
end

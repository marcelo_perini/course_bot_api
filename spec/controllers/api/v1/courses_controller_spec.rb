require 'rails_helper'

RSpec.describe Api::V1::CoursesController, type: :controller do
  before do
    TWITTER_CLIENT = double('twitter')
    allow(TWITTER_CLIENT).to receive(:update)
  end

  describe 'POST #create' do
    before { post :create, params: { course: course_attributes } }

    context 'with valid attributes' do
      let(:course_attributes) { attributes_for(:course) }
      let(:tweet_message) { course_attributes.custom_message }

      it 'renders created http status' do
        expect(response).to have_http_status(:created)
      end

      it 'instantiate course and persiste' do
        expect(assigns(:course)).to be_persisted
      end

      it 'save the correct course' do
        expect(assigns(:course)).to eq(Course.last)
      end

      it 'responds with JSON' do
        expect(response.body).to match(/"success":true/)
      end
    end

    context 'with invalid attributes' do
      let(:course_attributes) { attributes_for(:course, title: nil) }

      it 'renders bad request http status' do
        expect(response).to have_http_status(:bad_request)
      end

      it 'instantiate course and not persiste' do
        expect(assigns(:course)).not_to be_persisted
      end

      it 'responds with JSON' do
        expect(response.body).to match(/"success":false/)
      end
    end
  end
end

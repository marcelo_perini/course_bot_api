# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course, type: :model do
  subject { build(:course) }

  it { is_expected.to respond_to :title }
  it { is_expected.to respond_to :description }
  it { is_expected.to respond_to :course_url }
  it { is_expected.to respond_to :time_available }
  it { is_expected.to respond_to :is_paid }
  it { is_expected.to respond_to :price }
  it { is_expected.to respond_to :workload }
  it { is_expected.to respond_to :custom_message }
  it { is_expected.to respond_to :postage_time }

  it { is_expected.to be_valid }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:course_url) }
  it { is_expected.to validate_presence_of(:time_available) }
  it { is_expected.to validate_presence_of(:is_paid) }
  it { is_expected.to validate_presence_of(:price) }
  it { is_expected.to validate_presence_of(:workload) }

  it { is_expected.to validate_length_of(:title).is_at_most(100) }
  it { is_expected.to validate_length_of(:description).is_at_most(255) }
  it { is_expected.to validate_length_of(:course_url).is_at_most(50) }
  it { is_expected.to validate_length_of(:custom_message).is_at_most(281) }

  it { is_expected.to validate_numericality_of(:price).is_greater_than(0) }
  it { is_expected.to validate_numericality_of(:workload).is_greater_than(0) }
end

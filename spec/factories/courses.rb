# frozen_string_literal: true

FactoryBot.define do
  factory :course do
    sequence(:title) { |i| "title #{i}" }
    sequence(:description) { |i| "description #{i}" }
    sequence(:course_url) { |i| "https://eadbox.com/courses/#{i}" }
    time_available Time.now
    is_paid true
    price 100.00
    workload 40
    custom_message 'Example'
    postage_time Time.now
  end
end
